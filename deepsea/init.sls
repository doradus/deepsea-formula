# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - deepsea.install
  - deepsea.config
  - deepsea.service
